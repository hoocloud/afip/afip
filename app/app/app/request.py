from .server import app
import dns.resolver

import pprint

import flask

import urllib

import ipaddress
import validator_collection


class AppResolver(dns.resolver.Resolver):
    def __init__(self):
        super().__init__(self, configure=False)
        self.nameservers = app.config['NAMESERVERS']


class AppValidation(object):
    def __init__(self):
        pass

    @staticmethod
    def isIP(ip):
        try:
            ipaddress.ip_address(ip)
        except ValueError:
            return False
        return True

    @staticmethod
    def isFQDN(fqdn):
        return validator_collection.checkers.is_domain(fqdn)


class AppNetwork(object):
    def __init__(self):
        pass

    @staticmethod
    def getHostFromIP(IP):
        # check if IP is valid
        if not AppValidation.isIP(IP):
            return False
        resolver =  AppResolver()
        try:
            FQDN = str(resolver.resolve_address(IP)[0]).rstrip('.')
        except:
            app.logger.debug('IP {} can not be looked up to FQDN'.format( IP ))
            return IP
        else:
            app.logger.debug('IP {} can be looked up to FQDN {} '.format( IP, FQDN ))
        return FQDN if AppValidation.isFQDN(FQDN) else IP

    @staticmethod
    def getIPFromHost(FQDN):
        if not AppValidation.isFQDN(FQDN):
            return False
        resolver =  AppResolver()
        try:
            IP = str(resolver.resolve(FQDN,'A')[0])
        except:
            app.logger.debug('FQDN {} can not be looked up to IP'.format( FQDN ))
            return FQDN
        else:
            app.logger.debug('FQDN {} can be looked up to IP {} '.format( FQDN, IP ))
        return IP if AppValidation.isIP(IP) else FQDN

class AppRequest(object):
    def __init__(self,
            request):
        self.request = request

        app.logger.debug('request: \n{}\n\n'.format( pprint.pformat(self.request) ))
        app.logger.debug('request.environ: \n{}\n\n'.format( pprint.pformat(self.request.environ) ))
        app.logger.debug('request.url: {}'.format( self.request.url ))
        app.logger.debug('request.scheme: {}'.format( self.request.scheme ))
        app.logger.debug('request.referrer: {}'.format( self.request.referrer ))

        self.clientHost = self.getClientHost()
        self.serverScheme = self.getServerScheme()
        self.serverHost = self.getServerHost()
        self.serverPort = self.getServerPort()
        self.serverPath = self.getServerPath()
        self.serverQueryString = self.getServerQueryString()

    def getClientHost(self):
        # Host of client from GET
        clientHostFromRequest = self.request.args.get('clientHost', False)
        app.logger.info('clientHostFromRequest: {}'.format( clientHostFromRequest ))

        # get Host of client from its IP
        #remoteHost = AppNetwork.getHostFromIP(self.request.remote_addr)
        #app.logger.debug('request.remote_addr: {}'.format( self.request.remote_addr ))
        #app.logger.debug('remoteHost: {}'.format( remoteHost ))
    
        app.logger.debug('request.access_route: {}'.format( pprint.pformat(self.request.access_route) ))
        remoteHost = AppNetwork.getHostFromIP(self.request.access_route[-1])
        app.logger.debug('remoteHost: {}'.format( remoteHost ))

        remoteIP = AppNetwork.getIPFromHost(remoteHost)
        if remoteIP == self.request.remote_addr:
            app.logger.debug('remoteIP {} and request.remote_addr {} are equal'.format( remoteIP, self.request.remote_addr ))

        clientHost = clientHostFromRequest if clientHostFromRequest else remoteHost
        if ( not AppValidation.isFQDN(clientHost) ) and ( not AppValidation.isIP(clientHost) ): flask.abort (400, description = 'clientHost {} is not valid'.format( clientHost ))

        return str(clientHost)

    def getServerScheme(self):
        app.logger.info('scheme: {}'.format( self.request.scheme ))
        return self.request.scheme
    
    def getServerHost(self):
        serverHost = self.request.host

        app.logger.debug('request.referrer {}'.format( self.request.referrer ))
        app.logger.debug('request.host {}'.format( self.request.host ))

        # rudimental CSRF security check
        if self.request.referrer and self.request.referrer.replace(self.request.scheme + '//', '').split('/')[0] != serverHost:
            flask.abort (400, description = 'referrer {} is not redirecting to serverHost: {}'.format( self.request.referrer, serverHost ))

        if not AppValidation.isIP(serverHost) and not AppValidation.isFQDN(serverHost):
            flask.abort (400, description = 'serverHost {} is not valid'.format( serverHost ))
        return str(serverHost)

    def getServerPort(self):
        url_data = urllib.parse.urlparse(self.request.url)
        port = url_data.port or (443 if self.request.scheme == 'https' else 80)
        app.logger.info('port: {}'.format( port ))
        return port
    
    def getServerPath(self):
        app.logger.info('path: {}'.format( self.request.path ))
        return self.request.path

    def getServerQueryString(self):
        app.logger.info('query_string: {}'.format( self.request.query_string.decode('utf-8') ))
        return self.request.query_string.decode('utf-8')

