from .server import app
from .config import *
from .request import *

import flask
import pprint

## routes
@app.route('/app/boot.ipxe')
def render_boot_ipxe():

    #FIXME: set loglevel via get request, but only if flask runs in debug mode
    # (securit risk if debugging is possible in production mode)

    appRequest = AppRequest(flask.request)

    clientConfig = AppClientConfig(appRequest.clientHost)
    app.logger.debug('clientConfig: \n{}'.format( pprint.pformat(clientConfig.clientConfig) ))
    
    return flask.render_template('boot.ipxe.j2', 
            appRequest = appRequest,
            clientConfig = clientConfig,
            mimetype='text/plain',
            )

@app.route('/app/preseed.cfg')
def render_preseed_cfg():

    #FIXME: set loglevel via get request, but only if flask runs in debug mode
    # (securit risk if debugging is possible in production mode)

    appRequest = AppRequest(flask.request)

    clientConfig = AppClientConfig(appRequest.clientHost)
    app.logger.debug('clientConfig: \n{}'.format( pprint.pformat(clientConfig.clientConfig) ))
    
    return flask.render_template('preseed.cfg.j2', 
            appRequest = appRequest,
            clientConfig = clientConfig,
            mimetype='text/plain',
            )

@app.route('/app/preseed-early_command')
def render_preseed_early_command():

    #FIXME: set loglevel via get request, but only if flask runs in debug mode
    # (securit risk if debugging is possible in production mode)

    appRequest = AppRequest(flask.request)

    clientConfig = AppClientConfig(appRequest.clientHost)
    app.logger.debug('clientConfig: \n{}'.format( pprint.pformat(clientConfig.clientConfig) ))
    
    return flask.render_template('preseed-early_command.j2', 
            appRequest = appRequest,
            clientConfig = clientConfig,
            mimetype='text/plain',
            )

@app.route('/app/preseed-late_command')
def render_preseed_late_command():

    #FIXME: set loglevel via get request, but only if flask runs in debug mode
    # (securit risk if debugging is possible in production mode)

    appRequest = AppRequest(flask.request)

    clientConfig = AppClientConfig(appRequest.clientHost)
    app.logger.debug('clientConfig: \n{}'.format( pprint.pformat(clientConfig.clientConfig) ))
    
    return flask.render_template('preseed-late_command.j2', 
            appRequest = appRequest,
            clientConfig = clientConfig,
            mimetype='text/plain',
            )
